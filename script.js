
// function for machine aleatory
const random = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

// function to populate html with messages
const populate = (divId, txtMessage) => {
    const span = document.createElement('span');
    const textContent = txtMessage;
    span.innerText = textContent;
    const div = document.getElementById(divId);
    div.appendChild(span);
}

// function to get key by value
function getKeyByValue(object, value) {
    return Object.keys(object).find(key => object[key] === value);
}

// main event (play the game)
const play = (event) => {
    // clean divs with published choices and results
    document.querySelector('#userDiv').innerText = "";
    document.querySelector('#machineDiv').innerText = '';
    document.querySelector('#resultDiv').innerText = "";

    // get wich button child of div parent was clicked
    const isButton = event.target.nodeName === 'BUTTON';
    if (!isButton) {
        return;
    }
    const whichBtn = event.target.id;
    // console.log(whichBtn);

    // set object array (literal - dictionary with options)
    dicRoles = { 0: 'rock', 1: 'paper', 2: 'scissors' };

    // transforms key into int (from event which button) and put into user variable
    let user = parseInt(getKeyByValue(dicRoles, whichBtn));

    // machine must choose different option from user
    let machine = null;
    do {
        machine = random(0, 2);
    } while (machine === user);

    // compare the main 3 conditions, in cases machine vs human and vice versa
    let winner = null;
    if (user === 0 && machine === 1) {
        winner = "Machine wins! Paper wraps rock!";
    } else if (user === 1 && machine === 0) {
        winner = "User wins! Paper wraps rock!";
    } else if (user === 0 && machine === 2) {
        winner = "User wins! Rock breaks scissors!";
    } else if (user === 2 && machine === 0) {
        winner = "Machine wins! Rock breaks scissors!";
    } else if (user === 1 && machine === 2) {
        winner = "Machine wins! Scissors cuts paper!";
    } else if (user === 2 && machine === 1) {
        winner = "User wins! Scissors cuts paper!";
    } else {
        winner = "other conditions!!!"
    }

    // populates related divs with: users and machine choice, and result
    populate('userDiv', `User choice: ${dicRoles[user]}`);
    populate('machineDiv', `Machine choice: ${dicRoles[machine]}`);
    populate('resultDiv', winner);
}

// get parent which some button was clicked
const wrapper = document.getElementById('wrapper');

// function to discover which button was clicked, returning its id
const parentEvent = (event) => {
    const isButton = event.target.nodeName === 'BUTTON';
    if (!isButton) {
        return;
    }
    const whichBtn = `id: ${event.target.id.toString()}`
}

// event listener to play when some button pressed
wrapper.addEventListener('click', play);